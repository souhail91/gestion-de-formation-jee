<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_module";  %>
<%! String sousMenuActuel = "menu_module_controle";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Module <span>Controle_Note</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="controle_note" />">Controle_Note</a></li>
          <li class="active">Module</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Ajouter</h3>
        </div>
        <f:form method="post" action="saveControle_note" modelAttribute="controle_noteForm">
			
        <div class="panel-body">
        <c:if test="${success_controle_note != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_controle_note}
							</div>
						</c:if>
						
						<c:if test="${error_controle_note != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_controle_note}
							</div>
						</c:if>
			
			<div class="row ">
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        
                   <div class="form-group">
                    <label class="col-sm-3 control-label">Annee</label>
                    <div class="col-sm-8">
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNEE--</f:option>
					    <f:options items="${liste_annee}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="annee" cssClass="error"></f:errors>
                    </div>
            
                    </div>
                
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Controle</label>
                    <div class="col-sm-8">
                    <f:select path="controle" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN CONTROLE--</f:option>
					    <f:options items="${liste_controles}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="controle" cssClass="error"></f:errors>
                    </div>
                 
                    </div>
                    
                 
              
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                 <div class="form-group">
                    <label class="col-sm-3 control-label">Etudiant</label>
                    <div class="col-sm-8">
                    <f:select path="etudiant" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN ETUDIANT--</f:option>
					    <f:options items="${liste_etudiant}" itemValue="id" itemLabel="fullName" />
					</f:select>
                    <f:errors path="etudiant" cssClass="error"></f:errors>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Note <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                    <f:input path="note" cssClass="form-control" size="20"/>
                    <f:errors path="note" cssClass="error"></f:errors>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-3 control-label">Decision <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                    <f:input path="decision" cssClass="form-control" size="20"/>
                    <f:errors path="decision" cssClass="error"></f:errors>
                    </div>
                  </div>
                  
                      </div><!-- col-sm-6 -->
             
           
              
               
  </div>
           
              
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>

<script src="<c:url value="/assets/js/jquery.validate.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.maskedinput.min.js" />"></script>


<script>





jQuery(document).ready(function() {
	
	jQuery("#phone").mask("(+212) 06-99-99-99-99");

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
 

  });

</script>




</body>
</html>