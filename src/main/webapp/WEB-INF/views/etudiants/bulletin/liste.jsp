<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_bulletin";  %>

<jsp:include page="../../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/js/angular-chosen/chosen-spinner.css" />
</jsp:include>
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Etudiants <span>Bulletin</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Etudiants</a></li>
          <li class="active">Bulletin</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" ng-app="goFormation">
     
       <div class="row" ng-controller="NotesBulletinController">
        <div class="col-sm-4 col-md-3">
        
     
        
            <h4 class="subtitle mb5">Centre</h4>
            <div class="form-group">
        	   <select 
	        	   name="Centre" 
	        	   chosen 
	        	   ng-model="models.centre" 
	        	   ng-options="v.intitule for v in values.centre" 
	        	   data-placeholder="Selectionner un centre">
               </select>
               </div>
               
               <div class="mb20"></div>
               <h4 class="subtitle mb5">Année</h4>
               <div class="form-group">
        	   <select 
	        	   name="Annee" 
	        	   chosen 
	        	   data-placeholder="Selectionner une année" 
	        	   no-results-text="'Selectionner une année'"
	        	   ng-disabled="!formSetup.isAnneeReady" 
	        	   ng-model="models.annee" 
	        	   ng-options="v.intitule for v in values.annee">
               
               </select>
               </div>  
               <h4 class="subtitle mb5">Formation</h4>
   
               <div class="form-group">
        	   <select 
        	   		name="Formation" 
        	   		chosen 
        	   		data-placeholder="Selectionner une formation" 
        	   		no-results-text="'Selectionner une formation'"
        	   		ng-disabled="!formSetup.isFormationReady" 
        	   		ng-model="models.formation" 
        	   		ng-options="v.intitule for v in values.formation">
               
               </select>
               </div> 
               
               <div class="mb20"></div>
               
               <h4 class="subtitle mb5">Filière</h4>
   
               <div class="form-group">
        	   <select 
        	   		name="Filiere" 
        	   		chosen 
        	   		data-placeholder="Selectionner une filière" 
        	   		no-results-text="'Selectionner une filière'"
        	   		ng-disabled="!formSetup.isFiliereReady" 
        	   		ng-model="models.filiere" 
        	   		ng-options="v.intitule for v in values.filiere">
               
               </select>
               </div>
               
               
            
            <div class="mb20"></div>
            
            <h4 class="subtitle mb5">Niveau</h4>
   
               <div class="form-group">
        	   <select 
        	   		name="Niveau" 
        	   		chosen 
        	   		data-placeholder="Selectionner un niveau" 
        	   		no-results-text="'Selectionner un niveau'"
        	   		ng-disabled="!formSetup.isNiveauReady" 
        	   		ng-model="models.niveau" 
        	   		ng-options="v.intitule for v in values.niveau">
               
               </select>
               </div>
                           
               
               <div class="mb20"></div>
               
               <p class="text-right">
               		<button class="btn btn-success btn-lg" ng-click="searchStart()" ng-disabled="!isFormCompleted()">Recherche</button>
               </p>
            
            
       </div><!-- col-sm-4 -->
        <div class="col-sm-8 col-md-9">
        
        <div class="panel panel-default">
        <div class="panel-body">
        
   
                    <div class="table-responsive">
                       <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Etudiant</th>
                                <th>Email</th>
                                <th>Telephone</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <tr ng-repeat="student in students" ng-hide="loading">
                            		<td>{{student.id}}</td>
                            		<td><a target="_blank" href="<c:url value="/etudiants/bulletin/view" />?annee_id={{models.annee.id}}&etudiant_id={{student.id}}"><i class="fa fa-file"></i>
                            		 {{student.fullName}}</a></td>
                            		<td>{{student.email}}</td>
                            		<td>{{student.telephone}}</td>
                            	</tr>
                            	
                            	<tr ng-show="students.length <= 0 && !loading">
                            		<td colspan="3">Effectuer une recherche pour consulter le resultat</td>
                            	</tr>
                            	
                            	<tr>
                            		<td ng-show="loading" colspan="3" class="text-center">
                            			<img src="<c:url value="/assets/images/loaders/loader13.gif" />" alt="" />
                            		</td>
                            	</tr>
                            	
                            	
								
                            </tbody>
                        </table>
                        </div>

     
     </div></div>
     </div>
    
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />

<script>

var baseURL = "<c:url value="/" />";
var centres_list = ${liste_centre_json};

</script>

<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
	<jsp:param name="javascripts" value="/assets/bower_components/angularjs/angular.min.js" />
	<jsp:param name="javascripts" value="/assets/bower_components/angular-resource/angular-resource.min.js" />
	<jsp:param name="javascripts" value="/assets/js/angular-chosen/chosen.js" />
	<jsp:param name="javascripts" value="/assets/js/notes_module.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    
    
    //jQuery("select").chosen({'width':'100%','white-space':'nowrap'});
 

  });

  
</script>

</body>
</html>