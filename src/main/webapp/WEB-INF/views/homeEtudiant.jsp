<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%! String menuActuel = "menu_dashboard";  %>


<jsp:include page="layout/header.jsp" />
<jsp:include page="layout/leftpanel.jsp" />
<jsp:include page="layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-home"></i> GoFormation <span>Tableau de bord</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/" />">GoFormation</a></li>
          <li class="active">Tableau de bord</li>
        </ol>
      </div>
    </div>

<div class="contentpanel">
    

      
 <div class="row">
        
	<div class="col-sm-12">
		<h6>Bonjour ${e.fullName}</h6>
	</div>
      
 </div> 
    
   

  
</div><!-- ./contentpanel -->
    
<jsp:include page="layout/rightpanel.jsp" />
<jsp:include page="layout/footer.jsp" />
<!-- 
<script src="<c:url value="/assets/js/flot/jquery.flot.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.resize.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.spline.min.js" />"></script>
 -->
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>


<script>
  jQuery(document).ready(function() {
    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");
    
  });
 
</script>

</body>
</html>