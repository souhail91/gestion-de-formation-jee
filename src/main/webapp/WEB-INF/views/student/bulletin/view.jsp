<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "e_b";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i>Students<span>Bulletin</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Students</a></li>
          <li class="active">Bulletin</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" id="printable">
    
    <div class="panel panel-default">
        <div class="panel-body">
        
        <h4 class="text-primary text-center">BULLETIN DE NOTES</h4>
        
        
        	<table class="table">
        		<tr>
        			<td>
        			<p><strong>Nom et prénom:</strong> ${etudiant.nom} ${etudiant.prenom}</p>
                    <abbr title="Filière">Filière:</abbr> ${etudiant.filiere.intitule}
        			</td>
        			<td class="text-right">
        			<p><strong>Année universitaire:</strong> ${annee.intitule}</p>
                    <abbr title="Filière">Niveau:</abbr> ${etudiant.niveaux.intitule}
        			</td>
        		</tr>
        	</table>
        
            
            <br>
            
            <div class="table-responsive">
            
            
            
            
            
            <c:forEach items="${data}" var="semestre">
            <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" width="5%">Codes</th>
                <th class="text-center" width="30%">Modules<br>${semestre.key} EI</th>
                <th class="text-center" width="30%">Matières</th>
                <th class="text-center" width="5%">Coef</th>
                <th class="text-center" width="20%">Notes Matières</th>
                <th class="text-center" width="10%">Moy Module</th>
              </tr>
            </thead>
            <tbody>
            
            <c:if test="${empty semestre.getValue()}">
            	<tr>
            		<td colspan="6" class="text-center">Aucun module dans ce semestre</td>
            		
            	</tr>
            </c:if>
            <c:if test="${not empty semestre.getValue()}">
            
            <c:forEach items="${semestre.getValue()}" var="module">
              <tr>
                <td>
                    <div class="text-primary"><strong>${moduleCode.get(module.key)}</strong></div>
                </td>
                <td>${module.key}</td>
                <td>
                	<table class="table table-bordered">
                	<c:if test="${empty module.getValue()}">
                	<tr>
                		<td>--</td>
                	</tr>
                	</c:if>
                	<c:if test="${not empty module.getValue()}">
                	 <c:forEach items="${module.getValue()}" var="matiere">
                		<tr>
                			<td>${matiere.key}</td>
                		</tr>
                	</c:forEach>
                	</c:if>
                	</table>
                	
                </td>
                <td>
                	<table class="table table-bordered">
                	<c:if test="${empty module.getValue()}">
                	<tr>
                		<td>--</td>
                	</tr>
                	</c:if>
                	<c:if test="${not empty module.getValue()}">
                		<c:forEach items="${module.getValue()}" var="matiere">
                		<tr>
                			<td>${matiere.getValue().get("coef")}</td>
                		</tr>
                	</c:forEach>
                	</c:if>
                	</table>
                </td>
                <td>
                	<table class="table table-bordered">
                	<c:if test="${empty module.getValue()}">
                	<tr>
                		<td width="50%" class="text-center">--</td>
                		<td width="50%" class="text-center">--</td>
                	</tr>
                	</c:if>
                	
                	<c:if test="${not empty module.getValue()}">
                	<c:forEach items="${module.getValue()}" var="matiere">
                		<tr>
                			<td width="50%" class="text-center">
                			<c:if test="${empty matiere.getValue().get('note_1_coeff')}">
							    --
							</c:if>
							<c:if test="${not empty matiere.getValue().get('note_1_coeff')}">
							    ${matiere.getValue().get('note_1_coeff')}
							</c:if>
                			
                			</td>
                			<td width="50%" class="text-center">
                			<c:if test="${empty matiere.getValue().get('note_2_coeff')}">
							    --
							</c:if>
							<c:if test="${not empty matiere.getValue().get('note_2_coeff')}">
							    ${matiere.getValue().get("note_2_coeff")}
							</c:if>
                			</td>
                		</tr>
                		</c:forEach>
                		</c:if>
                		
                	</table>
                </td>
                <td>
                	<table class="table table-bordered">
                	<c:if test="${empty module.getValue()}">
                	<tr>
                		<td class="text-center">--</td>
                	</tr>
                	</c:if>
                	<c:if test="${not empty module.getValue()}">
                	<c:forEach items="${module.getValue()}" var="matiere">
                		<tr>
                			<td class="text-center">${matiere.getValue().get("moyen")}</td>
                		</tr>
                		</c:forEach>
               		</c:if>
                	</table>
                </td>
              </tr>
              <tr class="active">
              	<td colspan="3"></td>
              	<td colspan="2" class="text-right">Moyen Module</td>
              	<td class="text-center">
              		<c:if test = "${moduleMoyen.get(module.key) == 'NaN'}">
              			--
              		</c:if>
              		<c:if test = "${moduleMoyen.get(module.key) != 'NaN'}">
              			${moduleMoyen.get(module.key)}
              		</c:if>
              	
              	</td>
              </tr>
              
              </c:forEach>
              </c:if>
              
            </tbody>
          </table>
          </c:forEach>
          
          
          </div><!-- table-responsive -->
          
           
            
            <div class="text-right btn-invoice">
                <button class="btn btn-primary" id="print-btn"><i class="fa fa-print mr5"></i> Imprimer le bulletin</button>
            </div>
            
            <div class="mb40"></div>
            
            
            
        </div><!-- panel-body -->
      </div><!-- panel -->
     
 	</div>
 	<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
 	
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script>
  jQuery(document).ready(function($) {
   
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");
    
    var $head = $("iframe").contents().find("head");                
	$head.append($("<link/>", 
	    { rel: "stylesheet", href: "http://localhost:8080/<c:url value="/assets/css/print.css" />", type: "text/css" }));
    
    $("#print-btn").click(function(){
    	
    	
   	    window.frames["print_frame"].document.body.innerHTML= $("#printable").html();
   	    window.frames["print_frame"].window.focus();
   	    window.frames["print_frame"].window.print();
    	
    });
    
  });
  
</script>

</body>
</html>