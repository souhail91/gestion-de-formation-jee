<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "e_p";  %>

<jsp:include page="../../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/css/jquery.gritter.css" />
</jsp:include>
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />

<div class="pageheader">
      <h2><i class="fa fa-eur"></i> Paiment <span>Historique des Versements</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="versement" />">Paiment</a></li>
          <li class="active">Historique des Versements</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
         
          
        </div>
        <div class="panel-body">
        <c:if test="${success_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_delete}
							</div>
						</c:if>
						
						<c:if test="${error_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_delete}
							</div>
						</c:if>
						
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                   
	                     <th>Date Versement</th>
	                  
	                     <th>Type de paiement</th>   
	                    <th>Montant</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste_versements}">
								<tr>
         
                                    <td><joda:format pattern="MM/dd/yyyy" value="${v.dateSeance}" /></td>
                                 
                                    <td>${v.type}</td>
                                    <td>${v.montant} DHs</td>
                                </tr>
					</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

  jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>

</body>
</html>