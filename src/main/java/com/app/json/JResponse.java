package com.app.json;

public class JResponse {
	private String status;
	private String content;
	
	
	
	public JResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public JResponse(String status, String content) {
		super();
		this.status = status;
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
