package com.app.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.NotFoundException;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Controle;
import com.app.model.Controle_note;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Etudiant;
import com.app.model.Semestre;
import com.app.model.User;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.ControleService;
import com.app.service.Controle_noteService;
import com.app.service.EtudiantService;
import com.app.service.MatiereService;
import com.app.service.ModuleService;
import com.app.service.SemestreService;
import com.app.service.UserService;

@Controller
public class BulletinController {
	@Autowired
	AnneeService anneeService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	ControleService controleService;
	@Autowired
	Controle_noteService controle_noteService;
	@Autowired
	SemestreService semestreService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	MatiereService matiereService;
	@Autowired
	CentreService centreSvc;
	@Autowired
	UserService userservice;
	
	@RequestMapping(value = "/etudiants/bulletin", method = RequestMethod.GET)
	public String index(Model model) 
	{
		
		ObjectMapper mapper = new ObjectMapper();
		List<Centre> list = centreSvc.getAll();
		
		String json = "";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		model.addAttribute("liste_centre_json", json);
	
		
		return "/etudiants/bulletin/liste";
	}
	
	@SuppressWarnings({ "unused", "unchecked", "static-access" })
	@RequestMapping(value = "/etudiants/bulletin/view", method = RequestMethod.GET)
	public String view(
			Model model,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "etudiant_id", required = true, defaultValue = "0") int etudiant_id
		) throws NotFoundException {
		
		model=generate(model, annee_id, etudiant_id);
		return "etudiants/bulletin/view";
	}
	
	@PreAuthorize("hasRole('ETUDIANT')")
	@RequestMapping(value = "/student/bulletin/view", method = RequestMethod.GET)
	public String viewStudent(
			Model model
			
		) throws NotFoundException {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u;
		try {
			u = userservice.findByUsername(userDetails.getUsername());
			Etudiant e = etudiantService.findByUser(u);
		model=generate(model, e.getAnnee().getId(), e.getId());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return "student/bulletin/view";
	}
	private Model generate(Model model,int annee_id,int etudiant_id) throws NotFoundException{
		
	Annee an = anneeService.findById(annee_id);
		
		
		Etudiant et = etudiantService.findById(etudiant_id);
		Niveaux niv = et.getNiveaux();
		
		List<Semestre> semestresList = semestreService.findByAnnee(an);
		
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Double> moduleMoyen = new HashMap<String, Double>();
		Map<String, String> moduleCode = new HashMap<String, String>();
		
		for(Semestre s : semestresList){
			data.put(s.getIntitule(), new HashMap<String, Object>());
			
			List<Module> modulesList = moduleService.findBySemestre(s);
			
			
			for(Module m : modulesList)
			{
				moduleCode.put(m.getIntitule(), m.getContextuel());
				Map<String, Object> map_semestre = ((Map<String, Object>) data.get(s.getIntitule()));
				map_semestre.put(m.getIntitule(), new HashMap<String, Object>());
				
				List<Matiere> matieresList = matiereService.findByModule(m);
				
				int nbModuleCoef = 0;
				List<Double> tableNotes = new ArrayList<Double>();
				List<Integer> tableCoef = new ArrayList<Integer>();
				
				for(Matiere ma : matieresList){
					Map<String, Object> map_module = ((Map<String, Object>)  map_semestre.get(m.getIntitule()));
					
					
					Map<String, String> map_matiere = new HashMap<String, String>();
					map_matiere.put("coef", ma.getCoeff()+"");
					tableCoef.add(ma.getCoeff());
					
					//map_matiere.put("moyen", ma.getCoeff()+"");
					
					List<Controle> controlesList = controleService.findByMatiere(ma);
					int nbCoef = 0;
					for(Controle c : controlesList){
						if(c.getNiveaux().getId() == niv.getId() && c.getModule().getId() == m.getId())
						{ // Only this niveau & matiere
							nbCoef += ma.getCoeff();
							nbModuleCoef += ma.getCoeff();
							
							
							Controle_note bnote = controle_noteService.findByControleEtudiant(c, et);
							Double note = (bnote == null) ? 0.0 : bnote.getNote();
							
							//map_matiere.put("note_1", "");
							//map_matiere.put("note_2", "");
							if(c.getNumControle().ordinal() == 0)
							{
								map_matiere.put("note_1", note+"");
								map_matiere.put("note_1_coeff", (note*ma.getCoeff())+"");
							}
							if(c.getNumControle().ordinal() == 1)
							{
								map_matiere.put("note_2", note+"");
								map_matiere.put("note_2_coeff", (note*ma.getCoeff())+"");
							}
						}
					}
					map_matiere.put("nbCoef", nbCoef+"");
					Double note1 = 0.0;
					if(map_matiere.get("note_1_coeff") != null)
						note1 = Double.valueOf(map_matiere.get("note_1_coeff"));
						
					Double note2 = 0.0;
					if(map_matiere.get("note_2_coeff") != null)
						note2 = Double.valueOf(map_matiere.get("note_2_coeff"));
					
					
					if(nbCoef > 0){
						map_matiere.put("moyen", ""+((note1+note2)/nbCoef));
						tableNotes.add(((note1+note2)/nbCoef));
					}
					else{
						map_matiere.put("moyen", ""+(note1+note2));
						tableNotes.add(note1+note2);
					}

				
					map_module.put(ma.getIntitule(), map_matiere);

				}
				
				
				// Moyen des notes
				Double sommeNotes = 0.0;
				System.out.println("******** Module : "+m.getIntitule());
				
				System.out.println("nbcoef module: "+nbModuleCoef);
				nbModuleCoef = 0;
				for(int i=0;i<tableNotes.size();i++){
					System.out.println("Coef "+tableCoef.get(i));
					System.out.println("Note "+tableNotes.get(i));
					System.out.println("Mul "+tableNotes.get(i)*tableCoef.get(i));
					nbModuleCoef += tableCoef.get(i);
					sommeNotes += tableNotes.get(i)*tableCoef.get(i);
				}
				System.out.println("Total Moen: "+nbModuleCoef);
				System.out.println("Somme : "+sommeNotes);
				System.out.println("Moyen :" +sommeNotes/nbModuleCoef);
				System.out.println("////// *******");
				moduleMoyen.put(m.getIntitule(), sommeNotes/nbModuleCoef);
				
			}
			
			/*List<Controle> controlesList = controleService.findBySemestre(s);
			
			for(Controle c : controlesList){
				if(c.getNiveaux().getId() == niv.getId())
				{ // Only this niveau
					
					Controle_note bnote = controle_noteService.findByControleEtudiant(c, et);
					
				}
			}*/
		}
		
		// LOOP SEMESTRE
		for (Map.Entry<String, Object> s : data.entrySet())
		{
			System.out.println("-Semestre: "+s.getKey() );
		    Map<String, Object> modules = (HashMap<String, Object>) s.getValue();
		    // LOOP MODULES
		    for (Map.Entry<String, Object> mo : modules.entrySet())
		    {
		    	System.out.println("--Module: "+mo.getKey() );
		    	System.out.println("--** Moyen Module: "+moduleMoyen.get(mo.getKey()));
		    	Map<String, Object> matieres = (HashMap<String, Object>) mo.getValue();
		    	
		    	// LOOP MATIERES
		    	for (Map.Entry<String, Object> ma : matieres.entrySet())
		    	{
		    		System.out.println("---Matiere: "+ma.getKey() );
		    		Map<String, String> matiere = (HashMap<String, String>) ma.getValue();
		    		System.out.println("--- Coef: "+matiere.get("coef"));
		    		System.out.println("--- Note 1: "+matiere.get("note_1"));
		    		System.out.println("--- Note 2: "+matiere.get("note_2"));
		    		System.out.println("--- Note Coef 1: "+matiere.get("note_1_coeff"));
		    		System.out.println("--- Note Coef 2: "+matiere.get("note_2_coeff"));
		    		System.out.println("--- Moyen: "+matiere.get("moyen"));
		    		System.out.println("--- nbCoef: "+matiere.get("nbCoef"));
		    		
		    	}
		    	
		    }
		}
		
		model.addAttribute("data", data);
		model.addAttribute("moduleMoyen", moduleMoyen);
		model.addAttribute("moduleCode", moduleCode);
		model.addAttribute("etudiant", et);
		model.addAttribute("annee", an);
		
		return model;
	}	
	}
	

	