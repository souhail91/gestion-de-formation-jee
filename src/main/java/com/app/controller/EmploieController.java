package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.security.SecureRandom;
import java.util.Arrays;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;








import com.app.model.Annee;
import com.app.model.Emploie_Temps;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.service.AnneeService;
import com.app.service.EmploieService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;


@Controller
public class EmploieController {
	@Autowired
	EmploieService emploieService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	AnneeService anneeService;
	@Autowired
	FormationService formationService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	SemestreService semestreService;
	@Autowired
	LogTrackerService logSvc;
	
private	String [] validExtensions = {"doc","docx","pdf","jpg","png","jpeg"};
	
	   private static String getFileExtension(String fileName) {
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	
	   
		public static boolean useList(String[] arr, String ext) {
			return Arrays.asList(arr).contains(ext);
		}
	
		
		
		@InitBinder
	    public void initBinder(WebDataBinder binder) {
	        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
	        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
	        binder.registerCustomEditor(Annee.class, new AnneeEditor());
	        binder.registerCustomEditor(Formation.class, new FormationEditor());
	        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
	    }
		
		
		@RequestMapping(value="/etudiants/emploie/dowload")
		public void getLogFile(HttpSession session,HttpServletResponse response,
				@RequestParam(value = "file", required = true, defaultValue = "0") String fileName

				) throws Exception {
		    try {
		    	 String rootPath = System.getProperty("catalina.base");
		            File dir = new File(rootPath + File.separator + "uploads/emploie");
		    	
		        String filePathToBeServed = dir.getAbsolutePath()+ File.separator + fileName;
		        File fileToDownload = new File(filePathToBeServed);
		        InputStream inputStream = new FileInputStream(fileToDownload);
		        
		        response.setContentType("application/force-download");
		        response.setHeader("Content-Disposition", "attachment; filename="+fileName); 
		        IOUtils.copy(inputStream, response.getOutputStream());
		        response.flushBuffer();
		        inputStream.close();
		      
		    } catch (Exception e){
		        e.printStackTrace();
		    }

		}
		
		
		
		
		
		@PreAuthorize("hasRole('EMPLOIE_READ')")

	@RequestMapping(value = "/etudiants/emploie", method = RequestMethod.GET)
	public String index(Model model) {
        		
		model.addAttribute("liste_emploies", emploieService.getAll());
	

		return "etudiants/emploie/home";
	}
	
		@PreAuthorize("hasRole('EMPLOIE_EDIT')")
	@RequestMapping(value = "/etudiants/emploie/ajouter", method = RequestMethod.GET)
	public String ajouterEmploie(Model model) {
		
		model.addAttribute("emploieForm", new Emploie_Temps());
		model.addAttribute("liste_emploies",emploieService.getAll());
		
		model.addAttribute("liste_niveaux", niveauxService.getAll());
		model.addAttribute("liste_annees", anneeService.getAll());
		model.addAttribute("liste_formations", formationService.getAll());
		model.addAttribute("liste_filieres", filiereService.getAll());
		model.addAttribute("liste_semestres", semestreService.getAll());
		
		return "etudiants/emploie/ajouter";
	}
	
	
	
	
	
		@PreAuthorize("hasRole('EMPLOIE_EDIT')")
	@RequestMapping(value = "/etudiants/emploie/save", method = RequestMethod.POST)
	
	//@RequestMapping(value = "/etudiants/emploie/save", headers = "content-type=multipart/*", method = RequestMethod.POST)

	
	public String saveEmploie(
			@Valid @ModelAttribute("emploieForm") Emploie_Temps p,
			BindingResult bindingResult,
			@RequestParam("intitule") String intitule,
			@RequestParam("annee") Annee annee,
			@RequestParam("formation") Formation formation,
			@RequestParam("filiere") Filiere filiere,
			@RequestParam("niveau") Niveaux niveau,
			@RequestParam("semestre") Semestre semestre,
			Model model,
			@RequestParam("file") MultipartFile file,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		if (bindingResult.hasErrors()) {
		
			System.out.println(bindingResult.toString());
			model.addAttribute("emploieForm", new Emploie_Temps());
			model.addAttribute("liste_emploies",emploieService.getAll());
			
			model.addAttribute("liste_niveaux", niveauxService.getAll());
			model.addAttribute("liste_annees", anneeService.getAll());
			model.addAttribute("liste_formations", formationService.getAll());
			model.addAttribute("liste_filieres", filiereService.getAll());
			model.addAttribute("liste_semestres", semestreService.getAll());
			return "etudiants/emploie/ajouter";
		}else{		
		
		
		
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("error", "Veuilez charger un fichier avec l'une des extensions : doc,docx,pdf,jpg,png,jpeg");
			return "redirect:/etudiants/emploie/ajouter";
		}
		else
		{
				// Creation des dossiers si inexsistant
	            String rootPath = System.getProperty("catalina.base");
	            File dir = new File(rootPath + File.separator + "uploads/emploie");
	            if (!dir.exists())
	                dir.mkdirs();
	            /*System.out.println("*****************************************************");
	            System.out.println(rootPath + File.separator + "uploads/emploie");*/
	            
				// Nom aleatoire
				SecureRandom random = new SecureRandom();
				String filename = new BigInteger(130, random).toString(32)+"."+getFileExtension(file.getOriginalFilename());
				//System.out.println("AAAAAAAAAAAAAAAA"+getFileExtension(file.getOriginalFilename()));
				Emploie_Temps dpUpdated = null;
				String extension = getFileExtension(file.getOriginalFilename());

				if(useList(validExtensions,extension)){
				try {
					// MD5 FILE
	                byte[] bytes = file.getBytes();
	                BufferedOutputStream stream =
	                        new BufferedOutputStream(new FileOutputStream(new File(dir.getAbsolutePath()
	                                + File.separator + filename)));
	                stream.write(bytes);
	                stream.close();
	                
	                Emploie_Temps dp = new Emploie_Temps();
	                dp.setIntitule(intitule);
	                dp.setDate_creation(new DateTime());
	                dp.setPath(filename);
	                dp.setAnnee(annee);
	                dp.setFormation(formation);
	                dp.setFiliere(filiere);
	                dp.setNiveau(niveau);
	                dp.setSemestre(semestre);
	                dpUpdated = emploieService.add(dp);
	            	logSvc.store("Ajout emploie: "+dp, request);

				  } catch (Exception e) {
		            	redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
		    			return "redirect:/etudiants/emploie/ajouter";
		            }
					
					System.out.println(dpUpdated.getId()+" Created! ");
					   try {
							//emploieService.update(dpUpdated);
							redirectAttributes.addFlashAttribute("success", "Le fichier "+intitule+ "a ete charger avec success");
							return "redirect:/etudiants/emploie";
							
						} catch (Exception e) {
							redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
			    			return "redirect:/etudiants/emploie";
						}
			}
   
          
			
			
			else
			{
				redirectAttributes.addFlashAttribute("error", "FORMAT du fichier non reconnue ! " );
    			return "redirect:/etudiants/emploie/ajouter";
			}
			}
			
			   
		}
			
		}

		@PreAuthorize("hasRole('EMPLOIE_EDIT')")
	
	@RequestMapping(value = "/etudiants/emploie/modifier", method = RequestMethod.GET)
	public String modifierEmploie(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		
		
		Emploie_Temps p = emploieService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("emploieForm", p);
		
		
		
		model.addAttribute("liste_niveaux", niveauxService.getAll());
		model.addAttribute("liste_annees", anneeService.getAll());
		model.addAttribute("liste_formations", formationService.getAll());
		model.addAttribute("liste_filieres", filiereService.getAll());
		model.addAttribute("liste_semestres", semestreService.getAll());
		model.addAttribute("liste_emploie",emploieService.getAll());

		Emploie_Temps dp = emploieService.findById(id);
		
		
		model.addAttribute("emploie", dp);
		return "etudiants/emploie/modifier";
	}

	
	
		@PreAuthorize("hasRole('EMPLOIE_EDIT')")
	@RequestMapping(value = "/etudiants/emploie/update", method = RequestMethod.POST)
	public String update(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			@RequestParam(value = "intitule", required = true, defaultValue = "") String intitule,
			@RequestParam(value = "formation", required = true, defaultValue = "") Formation formation,
			@RequestParam(value = "filiere", required = true, defaultValue = "") Filiere filiere,
			@RequestParam(value = "niveau", required = true, defaultValue = "") Niveaux niveau,
			@RequestParam(value = "semestre", required = true, defaultValue = "") Semestre semestre,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		Emploie_Temps dp = emploieService.findById(id);
		
		dp.setIntitule(intitule);
		dp.setFormation(formation);
		dp.setFiliere(filiere);
		dp.setNiveau(niveau);
		dp.setSemestre(semestre);
		
		try {
			emploieService.update(dp);
        	logSvc.store("modification  emploie: "+dp, request);

			redirectAttributes.addFlashAttribute("success", "L'element "+intitule+" a ete mis a jour ");
			return "redirect:/etudiants/emploie/modifier?id="+id;
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
			return "redirect:/etudiants/emploie/modifier?id="+id;
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		@PreAuthorize("hasRole('EMPLOIE_EDIT')")
	@RequestMapping(value = "/etudiants/emploie/deleteEmploie", method = RequestMethod.GET)
	public String deleteEmploie(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,HttpServletRequest request) {
		try {
			Emploie_Temps p = emploieService.findById(id);
			emploieService.delete(id);
        	logSvc.store("Suppression  emploie: "+p, request);

			redirectAttributes.addFlashAttribute("success_emploie_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/etudiants/emploie";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_emploie_delete", "Entity not found");
			return "redirect:/etudiants/emploie";
		}
		
	}
}
