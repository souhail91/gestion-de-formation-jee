package com.app.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.model.Logtracker;
import com.app.service.LogTrackerService;

@Controller
public class LogController {
	@Autowired
	LogTrackerService logSvc;
	
	@RequestMapping(value = "/parametrage/journalisation", method = RequestMethod.GET)
	public String index() {
		return "parametrage/journalisation";
	}
	
	@RequestMapping(value = "/parametrage/downloadJournal", method = RequestMethod.POST)
	public ModelAndView download(
			@RequestParam(value = "date_start", required = true, defaultValue = "") String start,
			@RequestParam(value = "date_end", required = true, defaultValue = "") String end
			)  {
		
		System.out.println(start);
		System.out.println(end);
		
		List<Logtracker> l =logSvc.getByDate(start, end);
		if(l.size() > 0)
		{
			return new ModelAndView("LogsListExcel", "logsList", l);
		}
		else
		{
			return new ModelAndView("parametrage/journalisation", "error_journal", "Aucun resultat pour la date chosis");
		}
		
		
		//

	}
	
}
