package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.enums.PaiementType;
import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Etudiant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.model.User;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.PaiementTypeEditor;
import com.app.propertyeditors.UserEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.EtudiantService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.NiveauxService;
import com.app.service.UserService;

@Controller
public class EtudiantController {
	@Autowired
	LogTrackerService logSvc;
	
	@Autowired
	AnneeService anneeService;
	
	@Autowired
	CentreService centreService;
	@Autowired
	FormationService formationService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	EtudiantService etudiantService;
	
	@Autowired
	UserService userService;
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(PaiementType.class, new PaiementTypeEditor());
        binder.registerCustomEditor(User.class, new UserEditor());

    }
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_READ')")
	@RequestMapping(value = "/etudiants/etudiant", method = RequestMethod.GET)
	public String index(Model model) 
	{
	model.addAttribute("mode", "create");
		
		model.addAttribute("etudiantForm", new Etudiant());
		model.addAttribute("liste_etudiants", etudiantService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("utilisateurs", userService.getAll());

		

		
		return "etudiants/etudiant/liste";
	}
	

	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/etudiants/etudiant/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		
		model.addAttribute("etudiantForm", new Etudiant());
		
model.addAttribute("liste_etudiants", etudiantService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("types_paiement", PaiementType.values());
		model.addAttribute("utilisateurs", userService.getAll());
		
		return "etudiants/etudiant/ajouter";
	}
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/etudiants/etudiant/save", method = RequestMethod.POST)
	public String save(
			@Valid @ModelAttribute("etudiantForm") Etudiant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			
			model.addAttribute("etudiantForm", new Etudiant());
			
			model.addAttribute("liste_etudiants", etudiantService.getAll());
					
					model.addAttribute("annees", anneeService.getAll());
					model.addAttribute("centres", centreService.getAll());
					model.addAttribute("formations", formationService.getAll());
					model.addAttribute("filieres", filiereService.getAll());
					model.addAttribute("niveaux", niveauxService.getAll());
					model.addAttribute("types_paiement", PaiementType.values());
					model.addAttribute("utilisateurs", userService.getAll());
			
			return "etudiants/etudiant/ajouter";
		} else {
			etudiantService.add(p);
			logSvc.store("Ajout Etudiant: "+p, request);
			redirectAttributes.addFlashAttribute("success", "L'etudiant \""+p.getFullName()+"\" a ete bien ajouter");
			return "redirect:/etudiants/etudiant/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_READ')")
	@RequestMapping(value = "/etudiants/etudiant/editEtudiant", method = RequestMethod.GET)
	public String editEtudiant(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Etudiant p = etudiantService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("etudiantForm", p);
		
		model.addAttribute("liste_etudiants", etudiantService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("types_paiement", PaiementType.values());
		model.addAttribute("utilisateurs", userService.getAll());

		return "etudiants/etudiant/modifier";
	}
	
	
	
	
	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/etudiants/etudiant/updateEtudiant", method = RequestMethod.POST)
	public String updateEtudiant(
			@Valid @ModelAttribute("etudiantForm") Etudiant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("etudiantForm", p);
			model.addAttribute("liste_etudiants", etudiantService.getAll());
			
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationService.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("types_paiement", PaiementType.values());
			model.addAttribute("utilisateurs", userService.getAll());

			return "etudiants/etudiant/modifier";
		} else {
			
			try {
				etudiantService.update(p);
				logSvc.store("Mise a jour Etudiant: "+p, request);
				redirectAttributes.addFlashAttribute("success", p.getFullName() + " a ete bien modifier");
				return "redirect:/etudiants/etudiant/editEtudiant?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_matiere", "Entity not found");
				return "redirect:/etudiants/etudiant/editEtudiant?id="+p.getId();
			}
			
		}
	}
	
	
	
	
	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/etudiants/etudiant/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		
		try {
			Etudiant p = etudiantService.findById(id);
			etudiantService.delete(id);
			logSvc.store("Suppression Etudiant: "+p, request);
			redirectAttributes.addFlashAttribute("success_delete", "L'etudiant \""+p.getFullName()+"\" a ete bien supprimer");

			return "redirect:/etudiants/etudiant";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_delete", "Entity not found");
			return "redirect:/etudiants/etudiant";
		}
		
	}
	

}
