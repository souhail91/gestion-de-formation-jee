package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.EnseignantService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.UserService;

@Controller
public class ListeController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	EnseignantService enseignantService;
	@Autowired
	MatiereService matieretService;
	@Autowired
	UserService userService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	@PreAuthorize("hasRole('ETUDIANT')")
	@RequestMapping(value = "/student/professeurs", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", enseignantService.getAll());
		return "professeurs/liste";
	}
	
	
	
	

}
