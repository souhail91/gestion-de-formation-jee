package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Salle;
import com.app.service.AnneeService;
import com.app.service.LogTrackerService;
import com.app.service.SalleService;
import com.app.model.Centre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.service.CentreService;

@Controller
public class SalleController {
	@Autowired
	LogTrackerService logSvc;
	
	@Autowired
	SalleService SalleSvc;
	@Autowired
	CentreService centreSvc;
	@Autowired
	AnneeService anneeSvc;
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());

    }
	@PreAuthorize("hasRole('SALLE_READ')")
	@RequestMapping(value = "/parametrage/salles", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		model.addAttribute("centreForm", new Centre());
		model.addAttribute("liste_centre", centreSvc.getAll());
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("salleForm", new Salle());
		model.addAttribute("liste_salle", SalleSvc.getAll());
		
		return "parametrage/salles";
	}
	

	@PreAuthorize("hasRole('SALLE_EDIT')")
	@RequestMapping(value = "/parametrage/saveSalle", method = RequestMethod.POST)
	public String saveSalle(
			@Valid @ModelAttribute("salleForm") Salle v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			model.addAttribute("centreForm", new Centre());
			model.addAttribute("liste_centre", centreSvc.getAll());
			model.addAttribute("anneeForm", new Annee());
			model.addAttribute("liste_annee", anneeSvc.getAll());
			
			model.addAttribute("salleForm", new Salle());
			model.addAttribute("liste_salle", SalleSvc.getAll());
			return "parametrage/centre";
		} else {
			
		
			
			if(mode.equals("create"))
			{
				SalleSvc.add(v);
				logSvc.store("Ajout Salle: "+v, request);
				redirectAttributes.addFlashAttribute("success_salle", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/salles";
			}
			else{
				System.out.println("Updating "+v);
				try {
					SalleSvc.update(v);
					logSvc.store("Mise ajour Salle: "+v, request);
					redirectAttributes.addFlashAttribute("success_salle", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/salles";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_salle", "Entity not found");
					return "redirect:/parametrage/salles";
				}
				
			}
			
		}
	}
	

	@PreAuthorize("hasRole('SALLE_EDIT')")
	@RequestMapping(value = "/parametrage/editSalle", method = RequestMethod.GET)
	public String editSalle(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Salle p = SalleSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("salleForm", p);
		model.addAttribute("centreForm", new Centre());
		model.addAttribute("liste_centre", centreSvc.getAll());
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("liste_salle", SalleSvc.getAll());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		
		return "parametrage/salles";
	}
	

	
	@PreAuthorize("hasRole('SALLE_EDIT')")
	@RequestMapping(value = "/parametrage/deleteSalle", method = RequestMethod.GET)
	public String deleteSalle(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Salle p = SalleSvc.findById(id);
			SalleSvc.delete(id);
			logSvc.store("Suppression Salle: "+p, request);

			redirectAttributes.addFlashAttribute("success_salle_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/salles";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_salle_delete", "Entity not found");
			return "redirect:/parametrage/salles";
		}
		
	}
	
}
