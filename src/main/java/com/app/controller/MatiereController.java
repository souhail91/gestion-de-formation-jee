package com.app.controller;



import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.*;
import com.app.propertyeditors.*;
import com.app.service.*;

@Controller
public class MatiereController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	MatiereService matiereService;
	
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	@Autowired
	SemestreService semestreSvc;
	@Autowired
	FormationService formationSvc;

	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	ModuleService moduleService;
	
	@Autowired
	EnseignantService enseignantService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
        binder.registerCustomEditor(Enseignant.class, new EnseignantEditor());



    }
	
	@PreAuthorize("hasRole('MATIERE_READ')")
	@RequestMapping(value = "/module/matieres", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		
		model.addAttribute("matiereForm", new Matiere());
		model.addAttribute("liste_matieres", matiereService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("module", moduleService.getAll());
		model.addAttribute("enseignant", enseignantService.getAll());

		

		
		return "module/matieres/liste";
	}
	
	
	@PreAuthorize("hasRole('MATIERE_EDIT')")
	@RequestMapping(value = "/module/matieres/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("matiereForm", new Matiere());
	model.addAttribute("liste_matieres", matiereService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("module", moduleService.getAll());
		model.addAttribute("enseignant", enseignantService.getAll());

		return "module/matieres/ajouter";
	}
	
	@PreAuthorize("hasRole('MATIERE_EDIT')")
	@RequestMapping(value = "/module/matieres/saveMatiere", method = RequestMethod.POST)
	public String saveMatiere(
			@Valid @ModelAttribute("matiereForm") Matiere p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("matiereForm", p);
			
			model.addAttribute("liste_matieres", matiereService.getAll());
			
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationSvc.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("semestre", semestreSvc.getAll());
			model.addAttribute("module", moduleService.getAll());
			model.addAttribute("enseignant", enseignantService.getAll());

			
			return "module/matieres/ajouter";
		} else {
			
			matiereService.add(p);
			logSvc.store("Ajout Matiere: "+p, request);
			redirectAttributes.addFlashAttribute("success_matiere", p.getIntitule() + " a ete bien ajouter");
			return "redirect:/module/matieres/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('MATIERE_READ')")
	@RequestMapping(value = "/module/matieres/editMatiere", method = RequestMethod.GET)
	public String editMatiere(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Matiere p = matiereService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("matiereForm", p);
		
		model.addAttribute("liste_matieres", matiereService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("module", moduleService.getAll());
		model.addAttribute("enseignants", enseignantService.getAll());

		return "module/matieres/modifier";
	}
	
	@PreAuthorize("hasRole('MATIERE_EDIT')")
	@RequestMapping(value = "/module/matieres/updateMatiere", method = RequestMethod.POST)
	public String updateMatiere(
			@Valid @ModelAttribute("matiereForm") Matiere p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("matiereForm", p);
			model.addAttribute("liste_matieres", matiereService.getAll());
			
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationSvc.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("semestre", semestreSvc.getAll());
			model.addAttribute("module", moduleService.getAll());
			model.addAttribute("enseignants", enseignantService.getAll());

			return "module/matieres/modifier";
		} else {
			
			try {
				matiereService.update(p);
				logSvc.store("Mise a jour Matiere: "+p, request);
				redirectAttributes.addFlashAttribute("success_matiere", p.getIntitule() + " a ete bien modifier");
				return "redirect:/module/matieres/editMatiere?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_matiere", "Entity not found");
				return "redirect:/module/matieres/editMatiere?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('MATIERE_EDIT')")
	@RequestMapping(value = "/module/matieres/deleteMatiere", method = RequestMethod.GET)
	public String deleteMatiere(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Matiere p = matiereService.findById(id);
			matiereService.delete(id);
			logSvc.store("Suppression matiere: "+p, request);

			redirectAttributes.addFlashAttribute("success_matiere_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/module/matieres";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_matiere_delete", "Entity not found");
			return "redirect:/module/matieres";
		}
		
	}
	

}
