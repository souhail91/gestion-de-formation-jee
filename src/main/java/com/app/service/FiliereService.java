package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Filiere;
import com.app.model.Formation;

public class FiliereService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Filiere> getAll() {
		List<Filiere> result = em.createQuery("SELECT p FROM Filiere p",
				Filiere.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Filiere p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Filiere p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Filiere update(Filiere p) throws NotFoundException{
		Filiere up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Filiere findById(int id) {
		return em.find(Filiere.class, id);
	}
	
	@Transactional
	public List<Filiere> findByCentreAnneeFormation(Centre c, Annee a, Formation f) {
		List<Filiere> result = em.createQuery("SELECT p FROM Filiere p where  centre=:centre and annee=:annee and formation=:formation",
				Filiere.class)
				.setParameter("centre", c)
				.setParameter("annee", a)
				.setParameter("formation", f)
				.getResultList();
		return result;
	}
	
}