package com.app.service;

import java.util.List;
import javassist.NotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import com.app.model.Centre;

public class CentreService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Centre> getAll() {
		List<Centre> result = em.createQuery("SELECT p FROM Centre p",
				Centre.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Centre p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Centre p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Centre update(Centre p) throws NotFoundException{
		Centre up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Centre findById(int id) {
		return em.find(Centre.class, id);
	}
}