package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Enseignant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;

public class MatiereService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Matiere> getAll() {
		List<Matiere> result = em.createQuery("SELECT p FROM Matiere p",
				Matiere.class).getResultList();
		return result;
	}
	
	@Transactional
	public void add(Matiere p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Matiere p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Matiere update(Matiere p) throws NotFoundException{
		Matiere up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Matiere findById(int id) {
		return em.find(Matiere.class, id);
	}
	
	
	@Transactional
	public List<Matiere> findByMatiere(Enseignant ens)
            throws NotFoundException {
		List<Matiere> result = em.createQuery("SELECT p FROM Matiere p WHERE enseignant = :ens",
				Matiere.class).setParameter("ens", ens).getResultList();
	    
	    return result;
	}
	
	
	@Transactional
	public List<Matiere> findByCentreAnneeFormationFiliereNiveauSemestreModule(Centre c, Annee a, Formation f, Filiere fi, Niveaux n, Semestre s, Module m) {
		List<Matiere> result = em.createQuery("SELECT p FROM Matiere p where  centre=:centre and annee=:annee and formation=:formation and filiere=:filiere and niveaux=:niveaux and semestre=:semestre And module=:module",
				Matiere.class)
				.setParameter("centre", c)
				.setParameter("annee", a)
				.setParameter("formation", f)
				.setParameter("filiere", fi)
				.setParameter("niveaux", n)
				.setParameter("semestre", s)
				.setParameter("module", m)
				.getResultList();
		return result;
	}
	
	@Transactional
	public List<Matiere> findByModule(Module m)
            throws NotFoundException {
		List<Matiere> result = em.createQuery("SELECT p FROM Matiere p WHERE module = :module",
				Matiere.class).setParameter("module", m).getResultList();
	    
	    return result;
	}
	
}