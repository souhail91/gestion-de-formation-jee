package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Controle;
import com.app.model.Controle_note;
import com.app.model.Etudiant;


public class Controle_noteService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Controle_note> getAll() {
		List<Controle_note> result = em.createQuery("SELECT p FROM Controle_note p ",
				Controle_note.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Controle_note p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Controle_note p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Controle_note update(Controle_note p) throws NotFoundException{
		Controle_note up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Controle_note findById(int id) {
		return em.find(Controle_note.class, id);
	}
	
	
	
	@Transactional
	public Controle_note findByControleEtudiant(Controle controle,Etudiant etudiant)
            throws NotFoundException {
		List<Controle_note> result = em.createQuery("select o from Controle_note o  where o.etudiant=:etudiant  and o.controle= :controle",
				Controle_note.class).setParameter("etudiant", etudiant).setParameter("controle", controle).getResultList();
		
		if(result == null || result.size() <= 0 )
			return null;
	    
	    return result.get(0);
	}
	
}