package com.app.security;

import java.io.IOException;

import javassist.NotFoundException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.app.model.Etudiant;
import com.app.model.User;
import com.app.service.EtudiantService;
import com.app.service.UserService;

@Component
public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	@Autowired
	UserService userService;
	
	@Autowired
	EtudiantService etudiantService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, 
	                                    HttpServletResponse response,          
	                                    Authentication authentication) throws IOException,ServletException {

	    
	    UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
        System.out.println("Successed login:" + userDetails.getUsername());
        HttpSession session=request.getSession();
        try {
			User u = userService.findByUsername(userDetails.getUsername());
			u.setLastLogin(new DateTime());
			userService.update(u);
			session.setAttribute("user_connecte", u);
			session.setAttribute("user_id_connecte", u.getId());
			
			Etudiant e = etudiantService.findByUser(u);
			if(e != null)
			{
				session.setAttribute("etudiant_connecte", e);
			}
			
		} catch (NotFoundException e) {
			//e.printStackTrace();
		}
        
        super.onAuthenticationSuccess(request, response, authentication);
        
	  }
}
