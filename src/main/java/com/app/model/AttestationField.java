package com.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "documents_attestation_fields")
public class AttestationField implements Comparable<AttestationField>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_field")
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Attestation attestation;
	
	private String field;
	
	private String value;

	public AttestationField() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public Attestation getAttestation() {
		return attestation;
	}



	public void setAttestation(Attestation attestation) {
		this.attestation = attestation;
	}



	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(AttestationField o) {
		return o.getId() - this.id;
	}
	
	
	
	
}
