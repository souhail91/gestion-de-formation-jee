package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.enums.TypeVersement;

@Entity
@Table(name = "versement")
public class Versement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_versement")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_niveau")
	private Niveaux niveau;
	
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	
	
	@ManyToOne
	@JoinColumn(name="id_etudiant")
	private Etudiant etudiant;
	
	
	@Column(name="date_seance")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime dateSeance;
	
	
	@Column(name="type")
	@Enumerated(EnumType.ORDINAL)
	private TypeVersement type;
	

	@Column(name="montant")
	private double montant;


	public Versement() {
		super();
	}


	public Versement(int id, Annee annee, Centre centre, Formation formation,
			Filiere filiere, Niveaux niveau, Semestre semestre,
			Etudiant etudiant, DateTime dateSeance, TypeVersement type,
			double montant) {
		super();
		this.id = id;
		this.annee = annee;
		this.centre = centre;
		this.formation = formation;
		this.filiere = filiere;
		this.niveau = niveau;
		this.semestre = semestre;
		this.etudiant = etudiant;
		this.dateSeance = dateSeance;
		this.type = type;
		this.montant = montant;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Annee getAnnee() {
		return annee;
	}


	public void setAnnee(Annee annee) {
		this.annee = annee;
	}


	public Centre getCentre() {
		return centre;
	}


	public void setCentre(Centre centre) {
		this.centre = centre;
	}


	public Formation getFormation() {
		return formation;
	}


	public void setFormation(Formation formation) {
		this.formation = formation;
	}


	public Filiere getFiliere() {
		return filiere;
	}


	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}


	public Niveaux getNiveau() {
		return niveau;
	}


	public void setNiveau(Niveaux niveau) {
		this.niveau = niveau;
	}


	public Semestre getSemestre() {
		return semestre;
	}


	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}


	public Etudiant getEtudiant() {
		return etudiant;
	}


	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}


	public DateTime getDateSeance() {
		return dateSeance;
	}


	public void setDateSeance(DateTime dateSeance) {
		this.dateSeance = dateSeance;
	}


	public TypeVersement getType() {
		return type;
	}


	public void setType(TypeVersement type) {
		this.type = type;
	}


	public double getMontant() {
		return montant;
	}


	public void setMontant(double montant) {
		this.montant = montant;
	}


	@Override
	public String toString() {
		return dateSeance.toString("MM/dd/yyyy");
	}

}
