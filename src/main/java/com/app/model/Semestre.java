package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "semestre")
public class Semestre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_semestre")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 2, max = 20, message = "Le nom doit etre compris entre 2 et 20 caracteres")
	private String intitule;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_niveaux")
	private Niveaux niveaux;

	public Semestre() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public Centre getCentre() {
		return centre;
	}

	public void setCentre(Centre centre) {
		this.centre = centre;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Niveaux getNiveaux() {
		return niveaux;
	}

	public void setNiveaux(Niveaux niveaux) {
		this.niveaux = niveaux;
	}

	public Semestre(int id, String intitule, Annee annee, Centre centre,
			Formation formation, Filiere filiere, Niveaux niveaux) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.annee = annee;
		this.centre = centre;
		this.formation = formation;
		this.filiere = filiere;
		this.niveaux = niveaux;
	}

	@Override
	public String toString() {
		return intitule ;
	}
	

}
