package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Enseignant;

public class EnseignantEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
	@Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Enseignant c;
    	if(id == 0)
    		c = null;
    	else{
			c=  new Enseignant();
	    	c.setId(id);
    	}
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Enseignant c = (Enseignant) this.getValue();
    	if (c != null)
            return c.getFullname();
        return null;
    }

}
