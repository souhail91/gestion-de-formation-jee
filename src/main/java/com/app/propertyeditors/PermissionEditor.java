package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Permission;

public class PermissionEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Permission c = new Permission();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    @Override
    public String getAsText() {
    	Permission c = (Permission) this.getValue();
        return c.getName();
    }

}
