package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.enums.TypeVersement;



public class TypeVersementEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
        this.setValue(TypeVersement.valueOf(text));
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	TypeVersement c = (TypeVersement) this.getValue();
    	if (c != null)
            return c.name();
        return null;
    }

}
